# Malaga Jam Week VI - Soul Gun #

https://portfolio.visualstudioex3.com/2017/07/30/soul-gun/

![Soul Gun intro](https://i0.wp.com/portfolio.visualstudioex3.com/wp-content/uploads/2017/08/SoulGunLD1.gif?resize=474%2C296) ![Soul Gun gameplay](https://i2.wp.com/portfolio.visualstudioex3.com/wp-content/uploads/2017/08/SoulGunLD2.gif?resize=474%2C296)

A game made for the [__Malaga Jam Weekend VI__](https://itch.io/jam/mjwvi) & [__Ludum Dare 39__](https://ldjam.com/events/ludum-dare/39) Game Jams. A game made in 48h!

A 2D platform game with a gravity and energy mechanic. The player only can move on floor and shoot his weapon. 
Each shoot invert the gravity on entire level, enemies included, and each shoot consume a part of the player soul. 
If the player consumes his soul, then he will die instantly. The player can restore his soul when not shooting his weapon during a short time.

This game shared the __MalagaJam__ and __Ludum Dare__ themes: __Atraction__ & __Running out of Power__.

- itch.io: https://ex3.itch.io/soulgun
- Ludum Dare 39: https://ldjam.com/events/ludum-dare/39/soul-gun/ (510th position of +2500 games)

## CREDITS ##
### Graphic Designer ###
Pablo Dapena (Twitter: [@PabloDap](https://twitter.com/PabloDap))

### Game Designer ###
Carlos L. Hernando (Twitter: [@CarlosLHernando](https://twitter.com/CarlosLHernando))

### Game Designer, Level Designer ###
Alberto Sageras Roman (Twitter: [@AlbertoSajeras](https://twitter.com/AlbertoSajeras))

### Game Programmer ###
José Miguel Sánchez Fernández - [EX3] (Twitter: [@ex3_tlsa](https://twitter.com/ex3_tlsa))