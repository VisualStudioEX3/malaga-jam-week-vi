﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EX3.Framework;
using EX3.Framework.FSM;

public class FSMStateTemplate : FSMState
{
    /// <summary>
    /// Use this to initialize the state members and settings.
    /// </summary>
    public override void Initialize()
    {

    }

    /// <summary>
    /// Execute when the state is activate.
    /// </summary>
    public override void OnEnter()
    {

    }

    /// <summary>
    /// The logic state.
    /// </summary>
    public override void Update()
    {

    }

    /// <summary>
    /// Execute code before jump to other state.
    /// </summary>
    public override void OnExit()
    {

    }
}
