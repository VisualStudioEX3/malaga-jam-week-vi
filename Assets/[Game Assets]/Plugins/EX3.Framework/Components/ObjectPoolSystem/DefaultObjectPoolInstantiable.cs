﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EX3.Framework;

namespace EX3.Framework.Components
{
    /// <summary>
    /// Use this to setup a GameObject without script logic to be a instantiable object for an ObjectPool class.
    /// </summary>
    [AddComponentMenu("[EX3] Framework/Utils/Object Pool System/Default Object Pool Instantiable")]
    public class DefaultObjectPoolInstantiable : InstantiableObject
    {
    } 
}
