﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using UnityEngine;

namespace EX3.Framework
{
    /// <summary>
    /// Helper class.
    /// </summary>
    public static class Helper
    {
        /// <summary>
        /// Swap variable values.
        /// </summary>
        /// <typeparam name="T">Type of variables.</typeparam>
        /// <param name="a">First var.</param>
        /// <param name="b">Second var.</param>
        public static void Swap<T>(ref T a, ref T b)
        {
            T c = a; a = b; b = c;
        }

        /// <summary>
        /// Fill an array with a same value.
        /// </summary>
        /// <typeparam name="T">Array type data.</typeparam>
        /// <param name="array">Initialized array to fill.</param>
        /// <param name="value">Value to fill each element of array.</param>
        public static void Fill<T>(ref T[] array, T value)
        {
            array = Enumerable.Repeat<T>(value, array.Length).ToArray();
        }

        /// <summary>
        /// Fill a list with a same value.
        /// </summary>
        /// <typeparam name="T">List type data.</typeparam>
        /// <param name="list">Initialized list to fill.</param>
        /// <param name="value">Value to fill each element of list.</param>
        public static void Fill<T>(ref List<T> list, T value)
        {
            list = Enumerable.Repeat<T>(value, list.Count).ToList();
        }

        /// <summary>
        /// Created a safe random seed for intializing the System.Random class.
        /// </summary>
        /// <returns>Return a random seed.</returns>
        /// <remarks>This functions calculated the seed using the System.Security.Cryptography.RandomNumberGenerator.</remarks>
        public static int CalculateSafeRandomSeed()
        {
            var cryptoResult = new byte[4];
            new RNGCryptoServiceProvider().GetBytes(cryptoResult);
            return BitConverter.ToInt32(cryptoResult, 0);
        }

        /// <summary>
        /// Suffle an array.
        /// </summary>
        /// <typeparam name="T">Type of the array elements.</typeparam>
        /// <param name="array">Array of elements.</param>
        public static void Shuffle<T>(ref T[] array)
        {
            var rng = new System.Random(CalculateSafeRandomSeed());
            int n = array.Length;
            while (n > 1)
            {
                n--;
                int k = rng.Next(n + 1);
                T value = array[k];
                array[k] = array[n];
                array[n] = value;
            }
        }

        /// <summary>
        /// Suffle a list.
        /// </summary>
        /// <typeparam name="T">Type of the generic list elements.</typeparam>
        /// <param name="list">Generic list of elements.</param>
        public static void Shuffle<T>(ref List<T> list)
        {
            var rng = new System.Random(CalculateSafeRandomSeed());
            int n = list.Count;
            while (n > 1)
            {
                n--;
                int k = rng.Next(n + 1);
                T value = list[k];
                list[k] = list[n];
                list[n] = value;
            }
        }

        /// <summary>
        /// Transform the enum name element in to string value.
        /// </summary>
        /// <param name="value">Enum value.</param>
        /// <returns>Return the string name value.</returns>
        public static string EnumToString(object value)
        {
            return Enum.GetName(value.GetType(), value);
        }

        /// <summary>
        /// Generate a int32 value based on a GUID value.
        /// </summary>
        /// <returns></returns>
        public static int GenerateInt32GuidValue()
        {
            return BitConverter.ToInt32(Guid.NewGuid().ToByteArray(), 0);
        }

        /// <summary>
        /// Return a random element index on base of a probability table values.
        /// </summary>
        /// <param name="probabilityRatesTable">The array of probability with the occurrences for each element.</param>
        /// <returns>A random index based on its occurrence.</returns>
        /// <remarks>The probability rates table array contains values, from 0 to a max value to determine the occurrence of each element of an array. 
        /// This algorithm is based on a simbol rate generation code from a slot machine game.</remarks>
        public static int GetRandomIndexByProbabilityRate(int[] probabilityRatesTable)
        {
            int index;

            for (;;)
            {
                // Get random element from the list:
                index = UnityEngine.Random.Range(0, probabilityRatesTable.Length);

                // Calculate the max occurrence value, using the probability rate table array, and if its major than random value between 0 and 1, return the element index:
                if (((float)probabilityRatesTable[index] / (float)probabilityRatesTable.Length) >= UnityEngine.Random.value)
                {
                    return index;
                }
            }
        }

        /// <summary>
        /// Get unclamped angle between two vectors.
        /// </summary>
        /// <param name="a">First vector.</param>
        /// <param name="b">Second vector.</param>
        /// <returns>Return the unclampled angle.</returns>
        public static float GetAngle(Vector3 a, Vector3 b)
        {
            return Mathf.Atan2(b.y - a.y, b.x - a.x) / Mathf.PI * 180;
        }

        /// <summary>
        /// Determine if a value is in range.
        /// </summary>
        /// <param name="value">Value to check.</param>
        /// <param name="min">Lower bound of range.</param>
        /// <param name="max">Upper bound of range.</param>
        /// <returns></returns>
        public static bool IsValueInRange(int value, int min, int max)
        {
            return value >= min && value <= max;
        }

        /// <summary>
        /// Determine if a value is in range.
        /// </summary>
        /// <param name="value">Value to check.</param>
        /// <param name="min">Lower bound of range.</param>
        /// <param name="max">Upper bound of range.</param>
        /// <returns></returns>
        public static bool IsValueInRange(float value, float min, float max)
        {
            return value >= min && value <= max;
        }

        /// <summary>
        /// Determine if a float value is similar to a second float value, using tolerance value.
        /// </summary>
        /// <param name="a">First value.</param>
        /// <param name="b">Second value.</param>
        /// <param name="tolerance">Tolerance to similarity.</param>
        /// <returns>Return true if the first value is similar to second value.</returns>
        public static bool FloatComparasion(float a, float b, float tolerance)
        {
            return Mathf.Abs(a - b) < tolerance;
        }

        /// <summary>
        /// Calculate the percent from a value and range.
        /// </summary>
        /// <param name="value">Value to percent.</param>
        /// <param name="range">Range max value (e.g. 100)</param>
        /// <returns>Return the percent value.</returns>
        public static float PercentFromValue(int value, int range)
        {
            return value / range * 100;
        }

        /// <summary>
        /// Calculate the percent from a value and range.
        /// </summary>
        /// <param name="value">Value to percent.</param>
        /// <param name="range">Range max value (e.g. 100)</param>
        /// <returns>Return the percent value.</returns>
        public static float PercentFromValue(float value, float range)
        {
            return value / range * 100;
        }

        /// <summary>
        /// Calculate the value from a percent and range.
        /// </summary>
        /// <param name="percent">Percent value.</param>
        /// <param name="range">Range max value (e.g. 100)</param>
        /// <returns>Return the natural value.</returns>
        public static int ValueFromPercent(int percent, int range)
        {
            return percent * range / 100;
        }

        /// <summary>
        /// Calculate the value from a percent and range.
        /// </summary>
        /// <param name="percent">Percent value.</param>
        /// <param name="range">Range max value (e.g. 100)</param>
        /// <returns>Return the natural value.</returns>
        public static float ValueFromPercent(float percent, float range)
        {
            return percent * range / 100;
        }

        /// <summary>
        /// Set the ragdoll pose based on the character current pose.
        /// </summary>
        /// <param name="ragdoll">Ragdoll based on character.</param>
        /// <param name="character">Character to copy the pose.</param>
        public static void SetRagdollPose(Transform ragdoll, Transform character)
        {
            ragdoll.position = character.position;
            ragdoll.rotation = character.rotation;

            for (int i = 0; i < ragdoll.childCount; i++)
            {
                Helper.SetRagdollPose(ragdoll.GetChild(i), character.GetChild(i));
            }
        }
    }
}
